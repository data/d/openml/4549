# OpenML dataset: Buzzinsocialmedia_Twitter

https://www.openml.org/d/4549

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Creators :  François Kawala (1","2) Ahlame Douzal (1) Eric Gaussier (1) Eustache Diemert (2) Institutions :  (1) Université Joseph Fourier (Grenoble I) Laboratoire d'informatique de Grenoble (LIG) (2) BestofMedia Group Donor:  BestofMedia (ediemert '@' bestofmedia.com)  
**Source**: UCI 
**Please cite**: Pr&eacute;dictions d&rsquo;activit&eacute; dans les r&eacute;seaux sociaux en ligne (F. Kawala, A. Douzal-Chouakria, E. Gaussier, E. Dimert), In Actes de la Conf&eacute;rence sur les Mod&egrave;les et l&prime;Analyse des R&eacute;seaux : Approches Math&eacute;matiques et Informatique (MARAMI), pp. 16, 2013.  

Abstract: This data-set contains examples of buzz events from two different social networks: Twitter, and Tom's Hardware, a forum network focusing on new technology with more conservative dynamics.
Source:

Creators : 
Fran&ccedil;ois Kawala (1,2) Ahlame Douzal (1) Eric Gaussier (1) Eustache Diemert (2)
Institutions : 
(1) Universit&eacute; Joseph Fourier (Grenoble I)
Laboratoire d'informatique de Grenoble (LIG)
(2) BestofMedia Group
Donor: 
BestofMedia (ediemert '@' bestofmedia.com)


Data Set Information:

Please see [Web Link]


Attribute Information:

Please see [Web Link]


Relevant Papers:

Pr&eacute;dictions d&rsquo;activit&eacute; dans les r&eacute;seaux sociaux en ligne (F. Kawala, A. Douzal-Chouakria, E. Gaussier, E. Dimert), In Actes de la Conf&eacute;rence sur les Mod&egrave;les et l&prime;Analyse des R&eacute;seaux : Approches Math&eacute;matiques et Informatique (MARAMI), pp. 16, 2013.



Citation Request:

Pr&eacute;dictions d&rsquo;activit&eacute; dans les r&eacute;seaux sociaux en ligne (F. Kawala, A. Douzal-Chouakria, E. Gaussier, E. Dimert), In Actes de la Conf&eacute;rence sur les Mod&egrave;les et l&prime;Analyse des R&eacute;seaux : Approches Math&eacute;matiques et Informatique (MARAMI), pp. 16, 2013.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/4549) of an [OpenML dataset](https://www.openml.org/d/4549). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/4549/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/4549/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/4549/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

